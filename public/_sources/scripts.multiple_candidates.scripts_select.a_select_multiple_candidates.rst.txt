scripts\_select.a\_select\_multiple\_candidates subfolder
=========================================================

.. automodule:: scripts.multiple_candidates.scripts_select.a_select_multiple_candidates

scripts\_select.a\_select\_multiple\_candidates.run script
----------------------------------------------------------

.. automodule:: scripts.multiple_candidates.scripts_select.a_select_multiple_candidates.run

scripts\_select.a\_select\_multiple\_candidates.config module
-------------------------------------------------------------

.. automodule:: scripts.multiple_candidates.scripts_select.a_select_multiple_candidates.config
   :members:
   :undoc-members:
   :show-inheritance:
