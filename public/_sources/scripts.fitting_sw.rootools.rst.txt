fitting\_sw.rootools package
============================

.. automodule:: fitting_sw.rootools
   :members:
   :undoc-members:
   :show-inheritance:


fitting\_sw.rootools.fits module
--------------------------------

.. automodule:: fitting_sw.rootools.fits
   :members:
   :undoc-members:
   :show-inheritance:

fitting\_sw.rootools.models module
----------------------------------

.. automodule:: fitting_sw.rootools.models
   :members:
   :undoc-members:
   :show-inheritance:

fitting\_sw.rootools.plots module
---------------------------------

.. automodule:: fitting_sw.rootools.plots
   :members:
   :undoc-members:
   :show-inheritance:

