---------------
Getting started
---------------

.. toctree::
   :maxdepth: 4

   readme.overview
   readme.hammer
   readme.fittingforsw
   readme.snakemake
   readme.templatefitting
   readme.efficiency
   readme.rdstar
   readme.antiisotodefault
   readme.multiplecandidates
   readme.glossary
   readme.others
