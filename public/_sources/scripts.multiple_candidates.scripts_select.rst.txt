multiple\_candidates.scripts\_select subfolder
==============================================

.. automodule:: scripts.multiple_candidates.scripts_select

Subfolders
----------

.. toctree::
   :maxdepth: 4

   scripts.multiple_candidates.scripts_select.a_select_multiple_candidates
   scripts.multiple_candidates.scripts_select.b_compare_fit_results
   scripts.multiple_candidates.scripts_select.c_compute_chi2
